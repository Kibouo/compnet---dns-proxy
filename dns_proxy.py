from __future__ import with_statement

import socket as socket_mod
import struct as struct_mod
from datetime import datetime, timedelta
from time import sleep
from threading import Thread, Lock, ThreadError
from threading import Event as ThreadEvent
from string import split
from collections import OrderedDict
from sys import exit, argv
from parse_dns_name import parse_dns_name
from parse_hosts_file import parse_hosts_file


BYTES_TO_RECV = 512
DNS_PORT = 53
# use 0.0.0.0 as 127.0.0.1 is unable to reach outside
APPLICATION_IP = "0.0.0.0"
GOOGLE_DNS_IP = "8.8.8.8"
GOOGLE_DNS_SERVER_ADDRESS = (GOOGLE_DNS_IP, DNS_PORT)
DNS_HEADER_SIZE_BYTES = 12
MAX_SEC_WAIT_GOOGLE_DNS = 10.0
MAX_DELTA_WAIT_GOOGLE_DNS = timedelta(seconds=MAX_SEC_WAIT_GOOGLE_DNS)
# prevent sending from starving the receiver
MAX_SENDABLE = 16


class RecordBuilder():
    def __init__(self):
        # Only type=A according to assignment. This allows us to hardcode some
        # params.
        self.__rec_type = 1
        self.__rec_class = 1
        self.__ttl = 0
        # IPv4 byte length
        self.__rd_len = 4

    def build(self, domain_name, ip):
        enc_data = ""
        enc_data += self.__enc_domain_name(domain_name)
        enc_data += struct_mod.pack("!H", self.__rec_type)
        enc_data += struct_mod.pack("!H", self.__rec_class)
        enc_data += struct_mod.pack("!I", self.__ttl)
        enc_data += struct_mod.pack("!H", self.__rd_len)
        enc_ip = self.__enc_ip(ip)
        enc_data += enc_ip
        return DnsRecord(domain_name, self.__rec_type, self.__rec_class, self.__ttl, self.__rd_len, enc_ip, enc_data)

    def __enc_domain_name(self, domain_name):
        len_domain_tuples = map(lambda part: (len(part), part), split(domain_name, "."))

        byte_labels = ""
        for (l, p_str) in len_domain_tuples:
            byte_labels += struct_mod.pack("!B", l)
            byte_labels += "".join(map(lambda p: struct_mod.pack("!c", p), list(p_str)))

        # nullbyte
        byte_labels += struct_mod.pack("!B", 0)
        return byte_labels

    def __enc_ip(self, ip):
        return "".join(map(lambda p: struct_mod.pack("!B", int(p)), split(ip, ".")))


class DnsCache():
    def __init__(self, max_size=500):
        self.__lock = Lock()
        self.__max_size = max_size
        # cache values are in form:
        # ([ans], [auth], [add])
        self.__cache = OrderedDict()
        # We abuse the DnsCache to perform domain redirection.
        # The hosts cache is a cache consisting of data read from a hosts file
        # (hence the name). The hosts cache always has priority over the normal
        # cache.
        self.__hosts_cache = self.__data_from_hosts_file()

    def contains_and_get(self, question):
        question = question.to_cache_key()
        # Returns the value if present, otherwise None.
        with self.__lock:
            if question[0] in self.__hosts_cache:
                return self.__hosts_cache[question[0]]
            return self.__cache[question] if (question in self.__cache) else None

    def add_or_replace(self, question, records):
        question = question.to_cache_key()
        # Adds (key, value) to cache,
        # or replace value if key is already present.
        # Removes, abiding to FIFO, an entry if the cache is full.
        with self.__lock:
            self.__cache[question] = records
            if len(self.__cache) > self.__max_size:
                self.__cache.popitem(False)

    def __data_from_hosts_file(self):
        r_builder = RecordBuilder()
        return dict(map(lambda (d, ip): (d, ([r_builder.build(d, ip)], [], [])), parse_hosts_file("hosts").iteritems()))

class MessageQueue():
    def __init__(self):
        self.__queue=[]
        self.__lock=Lock()

    def push_back(self, msg, destination):
        # Pushes a (msg, destination) tuple to back of queue
        with self.__lock:
            self.__queue.append((msg, destination))

    def pop_front(self):
        # Pops a (msg, destination) tuple from the front of the queue and
        # returns it. Returns None if there is no item in the queue.
        with self.__lock:
            try:
                return self.__queue.pop(0)
            except IndexError:
                return None

    def dedup(self):
        # Our queue is not always fast enough, thus the threads start repeating
        # their questions. This just increases the length of the queue, making
        # the service time eventually approach infinity.
        with self.__lock:
            self.__queue=list(set(self.__queue))


class ConnectionStore():
    def __init__(self):
        self.__store={}
        self.__lock=Lock()

    def add_and_check_new(self, address, data):
        # Add or append data to the dictionary, using address as key.
        # Returns whether the key already existed (true) or not (false).
        with self.__lock:
            if address in self.__store:
                self.__store[address] += data
                return False
            else:
                self.__store[address]=data
                return True

    def peek_max_x_bytes(self, address, byte_amt=None):
        with self.__lock:
            if address in self.__store:
                if byte_amt is not None:
                    return self.__store[address][:byte_amt]
                else:
                    return self.__store[address]
            else:
                return ""

    def take_max_x_bytes(self, address, byte_amt=None):
        with self.__lock:
            if address in self.__store:
                if byte_amt is not None:
                    req_data=self.__store[address][:byte_amt]
                    self.__store[address]=self.__store[address][byte_amt:]
                    return req_data
                else:
                    req_data=self.__store[address]
                    self.__store[address]=""
                    return req_data
            else:
                return ""

    def check_data_and_delete_if_none(self, address):
        with self.__lock:
            has_data=address in self.__store and self.__store[address] != ""
            if not has_data:
                self.__store.pop(address, None)
            return has_data


class DnsMessage():
    def __init__(self, header, questions, answers, auth_rec, add_rec):
        self.header=header
        self.questions=questions
        self.answers=answers
        self.auth_rec=auth_rec
        self.add_rec=add_rec

    def is_query(self):
        return self.header.qr == "0"

    def to_bin(self):
        return self.header.to_bin() + self.__body_to_bin()

    def __body_to_bin(self):
        bin_data=""
        for q in self.questions:
            bin_data += q.to_bin()
        for a in self.answers:
            bin_data += a.to_bin()
        for ar in self.auth_rec:
            bin_data += ar.to_bin()
        for ad in self.add_rec:
            bin_data += ad.to_bin()
        return bin_data


class DnsHeader():
    def __init__(self, (id, flags, qd_count, an_count, ns_count, ar_count)):
        # NOTE:
        # 1. query/resp only -> 0 in resp/query respectively
        # 2. reserved        -> 0 always
        flags=bin(flags)[2:].zfill(16)
        # '0' -> msg is query
        # '1' -> msg is response
        self.qr=flags[0]
        # query only:
        # 0 -> std query
        # 1 -> inv query
        # 2 -> server status req
        # 3-15 (reserved)
        self.op_code=int(flags[1:5], 2)
        # resp only:
        # '0' -> server has no authority
        # '1' -> server has authority
        self.aa=flags[5]
        # '0' -> msg was not truncated
        # '1' -> msg was truncated
        self.tc=flags[6]
        # set in query, copy to resp
        # '0' -> no recursion desired
        # '1' -> recursion desired
        self.rd=flags[7]
        # resp only:
        # '0' -> no rec available
        # '1' -> rec available
        self.ra=flags[8]
        # (reserved)
        self.z=flags[9:12]
        # resp only:
        # 0 -> no error
        # 1 -> format error (invalid query)
        # 2 -> server error
        # 3 -> name error (only auth server; non-existing domain name)
        # 4 -> not implemented
        # 5 -> refused (due to some policy)
        # 6-15 (reserved)
        self.r_code=int(flags[12:16], 2)

        self.id=id
        # amt questions
        self.qd_count=qd_count
        # amt answers
        self.an_count=an_count
        # amt authority records
        self.ns_count=ns_count
        # amt additional records
        self.ar_count=ar_count

    def to_bin(self):
        flags=int("0b"
                    + self.qr
                    + bin(self.op_code)[2:].zfill(4)
                    + self.aa
                    + self.tc
                    + self.rd
                    + self.ra
                    + self.z
                    + bin(self.r_code)[2:].zfill(4), 2)
        return struct_mod.pack("!HHHHHH", self.id, flags, self.qd_count, self.an_count, self.ns_count, self.ar_count)


class DnsQuestion():
    def __init__(self, q_name_read, q_type, q_class, bin_data):
        self.q_name_read=q_name_read
        # A               1 IPv4 host address
        # CNAME           5 the canonical name for an alias
        # PTR             12 a domain name pointer
        # A               28 IPv6 host address
        self.q_type=q_type
        self.q_class=q_class
        self.bin_data=bin_data

    def to_cache_key(self):
        return (self.q_name_read, self.q_type, self.q_class)

    def to_bin(self):
        return self.bin_data


class DnsRecord():
    def __init__(self, rec_name_read, rec_type, rec_class, ttl, rd_len, r_data, bin_data):
        self.rec_name_read=rec_name_read
        # meaning of r_data
        self.rec_type=rec_type
        # class of r_data
        self.rec_class=rec_class
        # cache lifetime (sec)
        self.ttl=ttl
        # len of r_data
        self.rd_len=rd_len
        # Unencoded format
        self.r_data=r_data
        self.bin_data=bin_data

    def to_bin(self):
        return self.bin_data


class DnsMessageParser():
    # parses raw data into a message
    def __init__(self, key):
        self.key=key

    def parse(self):
        # Returns DnsMessage with at least the parsed header.
        # 2nd returned value indicates whether the body was parsed successfully.
        header_data, parsed_amt_bytes=self.__parse_head()
        header=DnsHeader(header_data)

        try:
            questions, answers, auth_rec, add_rec=self.__parse_body(
                header, parsed_amt_bytes)
            return (DnsMessage(header, questions, answers, auth_rec, add_rec), True)
        except IndexError:
            # We cannot determine the size of the message due to it failing to
            # parse. To prevent this error from occuring again, clear the
            # incoming data queue.
            global connection_store
            connection_store.take_max_x_bytes(self.key)
            return (DnsMessage(header, [], [], [], []), False)

    def __parse_head(self, offset=0):
        global connection_store
        total_parsed_bytes = 0

        data = connection_store.peek_max_x_bytes(
            self.key, DNS_HEADER_SIZE_BYTES)
        total_parsed_bytes += DNS_HEADER_SIZE_BYTES

        return struct_mod.unpack("!HHHHHH", data), total_parsed_bytes + offset

    def __parse_body(self, header, offset):
        global connection_store
        data = connection_store.peek_max_x_bytes(self.key)
        total_parsed_bytes = 0

        questions, parsed_bytes = self.__parse_questions(
            data, header.qd_count, total_parsed_bytes + offset)
        total_parsed_bytes += parsed_bytes

        answers, parsed_bytes = self.__parse_records(
            data, header.an_count, total_parsed_bytes + offset)
        total_parsed_bytes += parsed_bytes

        auth_rec, parsed_bytes = self.__parse_records(
            data, header.ns_count, total_parsed_bytes + offset)
        total_parsed_bytes += parsed_bytes

        add_rec, parsed_bytes = self.__parse_records(
            data, header.ar_count, total_parsed_bytes + offset)
        total_parsed_bytes += parsed_bytes

        connection_store.take_max_x_bytes(
            self.key, total_parsed_bytes + offset)
        return (questions, answers, auth_rec, add_rec)

    def __parse_questions(self, data, qd_count, offset):
        questions = []
        parsed_bytes = 0

        for _ in range(qd_count):
            start_byte = parsed_bytes + offset

            q_name_read, _, amt_bytes = parse_dns_name(
                data, parsed_bytes + offset)
            parsed_bytes += amt_bytes

            q_type = struct_mod.unpack(
                "!H", data[parsed_bytes + offset:parsed_bytes + offset+2])[0]
            parsed_bytes += 2

            q_class = struct_mod.unpack(
                "!H", data[parsed_bytes + offset:parsed_bytes + offset+2])[0]
            parsed_bytes += 2

            questions.append(DnsQuestion(
                q_name_read, q_type, q_class, data[start_byte:parsed_bytes + offset]))

        return (questions, parsed_bytes)

    def __parse_records(self, data, amt_records, offset):
        records = []
        parsed_bytes = 0

        for _ in range(amt_records):
            start_byte = parsed_bytes + offset

            rec_name_read, _, amt_bytes = parse_dns_name(
                data, parsed_bytes + offset)
            parsed_bytes += amt_bytes

            rec_type = struct_mod.unpack(
                "!H", data[parsed_bytes + offset:parsed_bytes + offset+2])[0]
            parsed_bytes += 2

            rec_class = struct_mod.unpack(
                "!H", data[parsed_bytes + offset:parsed_bytes + offset+2])[0]
            parsed_bytes += 2

            ttl = struct_mod.unpack(
                "!I", data[parsed_bytes + offset:parsed_bytes + offset+4])[0]
            parsed_bytes += 4

            rd_len = struct_mod.unpack(
                "!H", data[parsed_bytes + offset:parsed_bytes + offset+2])[0]
            parsed_bytes += 2

            enc = "!" + ("B" * rd_len)
            r_data = struct_mod.unpack(
                enc, data[parsed_bytes + offset:parsed_bytes + offset+rd_len])
            parsed_bytes += rd_len

            records.append(DnsRecord(rec_name_read, rec_type,
                                     rec_class, ttl, rd_len, r_data, data[start_byte:parsed_bytes + offset]))

        return (records, parsed_bytes)


class DnsMessageBuilder():
    def __init__(self, base_header, is_query, questions, answers=[], auth_rec=[], add_rec=[], error=0):
        self.base_header = base_header
        self.is_query = is_query
        self.questions = questions
        self.answers = answers
        self.auth_rec = auth_rec
        self.add_rec = add_rec
        self.error = error

    def build(self):
        id = self.base_header.id
        flags = self.__q_flags_from_q_flags(
        ) if self.is_query else self.__ans_flags_from_q_flags()
        qd_count = len(self.questions)
        an_count = len(self.answers)
        ns_count = len(self.auth_rec)
        ar_count = len(self.add_rec)
        header = DnsHeader((id, flags, qd_count, an_count, ns_count, ar_count))
        return DnsMessage(header, self.questions, self.answers, self.auth_rec, self.add_rec)

    def __q_flags_from_q_flags(self):
        flags = "0b" \
            + self.base_header.qr \
            + bin(self.base_header.op_code)[2:].zfill(4) \
            + self.base_header.aa \
            + self.base_header.tc \
            + self.base_header.rd \
            + self.base_header.ra \
            + self.base_header.z  \
            + bin(self.base_header.r_code)[2:].zfill(4)
        return int(flags, 2)

    def __ans_flags_from_q_flags(self):
        flags = "0b" \
            + "1" \
            + "0000" \
            + "0" \
            + "0" \
            + self.base_header.rd \
            + "1" \
            + self.base_header.z  \
            + bin(self.error)[2:].zfill(4)
        return int(flags, 2)


class ConnectionHandler(Thread):
    def __init__(self, address):
        Thread.__init__(self)
        self.address = address

    def run(self):
        # Executes on start of thread
        dns_message_parser = DnsMessageParser(self.address)
        global connection_store
        while connection_store.check_data_and_delete_if_none(self.address):
            dns_msg, body_parsing_success = dns_message_parser.parse()
            if body_parsing_success:
                if dns_msg.is_query():
                    self.__handle_query(dns_msg)
                else:
                    self.__handle_response(dns_msg)
            else:
                self.__handle_format_error(dns_msg)

    def __handle_query(self, msg):
        global dns_cache
        global msg_queue

        # Timer prevents thread from ending up in an infinite loop in case it
        # never gets an answer.
        start_process = datetime.now()
        now = start_process
        to_be_answered = msg.questions
        while (to_be_answered != []) and (now - start_process < MAX_DELTA_WAIT_GOOGLE_DNS):
            # 1. answer what we can
            cached_q_r_tuples = filter(
                lambda (q, r): r is not None,
                map(
                    lambda q: (q, dns_cache.contains_and_get(
                        q)),
                    msg.questions
                )
            )
            sendable_bin_msgs = map(lambda (q, r): DnsMessageBuilder(
                msg.header, False, [q], r[0], r[1], r[2]).build().to_bin(), cached_q_r_tuples)
            for bin_msg in sendable_bin_msgs:
                msg_queue.push_back(bin_msg, self.address)

            to_be_answered = [t for t in to_be_answered if t not in map(
                lambda (q, r): q, cached_q_r_tuples)]

            # 2. ask google for the queries we were unable to answer from cache
            # After this we wait for another thread to handle Google's response
            # and place the answers in cache. We will get back to answering the
            # questions after that.
            if len(to_be_answered) > 0:
                bin_msg = DnsMessageBuilder(
                    msg.header, True, to_be_answered).build().to_bin()
                msg_queue.push_back(bin_msg, GOOGLE_DNS_SERVER_ADDRESS)
                # Prevent thread from obnoxiously asking again before the network
                # even has a chance to answer the query.
                global querying_allowed
                querying_allowed.wait(MAX_SEC_WAIT_GOOGLE_DNS)

            now = datetime.now()

    def __handle_response(self, msg):
        # The student assumes that a response carries only answers for 1
        # question at a time. No documentation was found that denies or
        # confirms this. However, analysis via Wireshark did not show any
        # responses which answered more than 1 question at a time, so this
        # is not a bad assumption to make.
        # It would be nice to be informed (in class) about a potential weird
        # optimization which DOES allow an answer to respond to multiple
        # questions at once.
        global dns_cache
        global querying_allowed
        dns_cache.add_or_replace(
            msg.questions[0], (msg.answers, msg.auth_rec, msg.add_rec))
        # Indicate to thread which are waiting for a response from Google that
        # new data has been cached.
        querying_allowed.set()

    def __handle_format_error(self, msg):
        # send response message with error code back
        bin_msg = DnsMessageBuilder(
            msg.header, False, [], error=1).build().to_bin()
        msg_queue.push_back(bin_msg, self.address)


class ServerSocket:
    def __init__(self, ip_addr, port):
        self.ip_addr = ip_addr
        self.port = port

    def __enter__(self):
        try:
            self.start()
            return self
        except Exception as e:
            print e
            return None

    def start(self):
        self.socket = socket_mod.socket(
            socket_mod.AF_INET, socket_mod.SOCK_DGRAM)
        self.socket.bind((self.ip_addr, self.port))

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()

    def stop(self):
        self.socket.close()

    def handle_new_data(self):
        # Receives new data and starts new thread if it's from a
        # never-before seen client.
        global connection_store
        data, address = self.socket.recvfrom(BYTES_TO_RECV)

        if connection_store.add_and_check_new(address, data):
            return ConnectionHandler(address)
        else:
            return None

    def send_queued_messages(self):
        global msg_queue
        msg_queue.dedup()

        items_sent = 0
        item = msg_queue.pop_front()
        while (item is not None) and (items_sent < MAX_SENDABLE):
            data = item[0]
            addr = item[1]
            # while bytes to send
            while data != "":
                amt_bytes_sent = self.socket.sendto(data, addr)
                data = data[amt_bytes_sent:]
            item = msg_queue.pop_front()
            items_sent += 1


def join_threads(threads):
    for t in threads:
        t.join()


def delete_dead_threads(threads):
    return filter(lambda t: t.is_alive(), threads)


def main_loop(main_socket):
    connections = []
    try:
        while True:
            main_socket.send_queued_messages()
            new_conn = main_socket.handle_new_data()
            if new_conn is not None:
                connections.append(new_conn)
                new_conn.start()
            connections = delete_dead_threads(connections)

            # Through testing we discovered that the main thread consumes
            # relatively much CPU time. This CPU time has better use in the
            # slave threads for processing DNS messages.
            sleep(0.01)

    except KeyboardInterrupt:
        exit(0)
    finally:
        # No matter how the server stops running, connections threads should
        # be cleaned up to avoid orphans.
        join_threads(connections)


def main():
    with ServerSocket(APPLICATION_IP, DNS_PORT) as main_socket:
        if main_socket is not None:
            main_loop(main_socket)
        else:
            exit(1)


try:
    dns_cache = DnsCache(argv[1])
except IndexError:
    dns_cache = DnsCache()
connection_store = ConnectionStore()
msg_queue = MessageQueue()
querying_allowed = ThreadEvent()

main()
